QT += core script
QT -= gui

TARGET = taskParser
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    taskparser.cpp

HEADERS += \
    taskparser.h

