/*
 *
 */

#include "taskparser.h"

TaskParser::TaskParser() :
    listFile("taskList"),
    indexFile("taskIndex"),
    taskFile("taskFile"),
    outputFile("taskOutput")
{

}

TaskParser::~TaskParser()
{

}

bool TaskParser::initialize() {
    if(this->openFile(&listFile, QIODevice::ReadOnly) &&
            this->openFile(&indexFile, QIODevice::ReadOnly) &&
            this->openFile(&taskFile, QIODevice::ReadOnly) &&
            this->openFile(&outputFile, QIODevice::ReadWrite)) {
        indexStream.setDevice(&indexFile);
        indexStream.setByteOrder(QDataStream::LittleEndian);
        taskStream.setDevice(&taskFile);
        taskStream.setByteOrder(QDataStream::LittleEndian);
        indexDataStruct.clear();
        int counter = 0;
        while (indexStream.status() != QDataStream::ReadPastEnd) {
            indexStream >> index;
            if(counter%2 == 1) {
                indexDataStruct.append(index);
            }
            counter++;
        }
        indexFile.close();
        return true;
    }
    else return false;
}

bool TaskParser::processWithDataOutput() {
    outputDataStream.setDevice(&outputFile);
    outputDataStream.setByteOrder(QDataStream::LittleEndian);
    while(!listFile.atEnd()) {
        QByteArray taskLine = listFile.readLine();
        QList<QByteArray> operation = taskLine.split(' ');
        for(int i=0; i<operation.length()-1; i++) {
            taskFile.seek(indexDataStruct[operation[i].toInt()]);
            taskStream >> task;
            switch(task) {
            case -1:
                operationLine += " + ";
                break;
            case -2:
                operationLine += " - ";
                break;
            case -3:
                operationLine += " * ";
                break;
            case -4:
                operationLine += " / ";
                break;
            default:
                operationLine += QString::number(task);
                break;
            }
        }
        QString result = expression.evaluate(operationLine).toString();
        outputDataStream << result << endl;
        operationLine.clear();
    }
    if(this->closeAllFiles()) return true;
    else return false;
}

bool TaskParser::processWithTextOutput() {
    outputTextStream.setDevice(&outputFile);
    while(!listFile.atEnd()) {
        QByteArray taskLine = listFile.readLine();
        QList<QByteArray> operation = taskLine.split(' ');
        for(int i=0; i<operation.length()-1; i++) {
            taskFile.seek(indexDataStruct[operation[i].toInt()]);
            taskStream >> task;
            switch (task) {
            case -1:
                operationLine += " + ";
                break;
            case -2:
                operationLine += " - ";
                break;
            case -3:
                operationLine += " * ";
                break;
            case -4:
                operationLine += " / ";
                break;
            default:
                operationLine += QString::number(task);
                break;
            }
        }
        QString result = expression.evaluate(operationLine).toString();
        outputTextStream << result << endl;
        operationLine.clear();
    }
    if(this->closeAllFiles()) return true;
    else return false;
}

bool TaskParser::openFile(QFile *file, QIODevice::OpenMode mode) {
    if(!file->open(mode)) {
        qDebug() << "Cannot open" << file->fileName();
        return false;
    }
    return true;
}

bool TaskParser::closeAllFiles() {
    if(listFile.isOpen()) listFile.close();
    if(indexFile.isOpen()) indexFile.close();
    if(taskFile.isOpen()) taskFile.close();
    if(outputFile.isOpen()) outputFile.close();
    if(!listFile.isOpen() && !indexFile.isOpen() && !taskFile.isOpen() && !outputFile.isOpen()) {
        return true;
    }
    else return false;
}
