#include <QCoreApplication>
#include <QDateTime>

#include "taskparser.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qDebug() << "Start Time :\t" << QDateTime::currentDateTime().time();

    TaskParser *taskParser = new TaskParser();
    if(taskParser->initialize()) {
        if(taskParser->processWithTextOutput()) {
            delete taskParser;
            qDebug() << "Finish Time :\t" << QDateTime::currentDateTime().time();
            qDebug() << "Process Finished with plain text";
            exit(0);
        }
        /*if(taskParser->processWithDataOutput()) {
            delete taskParser;
            qDebug() << "Finish Time :\t" << QDateTime::currentDateTime().time();
            qDebug() << "Process Finished with octet-stream";
            exit(0);
        }*/
    }
    else {
        delete taskParser;
        qDebug() << "Error into process initialize !!";
        exit(-1);
    }

    return a.exec();
}

