#ifndef TASKPARSER_H
#define TASKPARSER_H

#include <QFile>
#include <QDataStream>
#include <QTextStream>
#include <QIODevice>
#include <QScriptEngine>
#include <QDebug>

class TaskParser
{
public:
    explicit TaskParser();
    ~TaskParser();

    bool initialize();

    bool processWithDataOutput();
    bool processWithTextOutput();

private:
    QFile listFile;
    QFile indexFile;
    QFile taskFile;

    QFile outputFile;

    QDataStream indexStream;
    QDataStream taskStream;

    QDataStream outputDataStream;
    QTextStream outputTextStream;

    QScriptEngine expression;

    qint32 index;
    qint32 task;

    QList<qint32> indexDataStruct;

    QString operationLine;

    bool openFile(QFile *file, QIODevice::OpenMode mode);
    bool closeAllFiles();
};

#endif // TASKPARSER_H

