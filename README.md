# taskParser

TaskParser

**<h1>How To Build Linux</h1>**

I used Ubuntu 16.04

**Before compilation:**
-   ~$ sudo apt install qtcreator qtcreator-dbg qtcreator-dev qtcreator-plugin-ubuntu qtscript5-dev qtscript5-dbg

**Build:**

Change directory to project folder.
-   ~$ qmake taskParser.pro
-   ~$ make

**Before run:**
-   copy taskList, taskIndex, taskFile to taskParser folder

**Note:**

If you want to plain-text output you can compile without making any changes.

But if you want to octet-stream output you must remove comments from main.cpp file in if(taskParser->initialize()) statement [line 20-25].
Then you must comment on line 14 to 19.